chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.cmd == "save") {
            console.log("save made it")
            
            chrome.downloads.download({
                url: request.data,
                filename: request.name + ".png",
                conflictAction: "overwrite"
            })

            const blob = new Blob([request.json], {type: "application/json"})
            chrome.downloads.download({
                url: URL.createObjectURL(blob),
                filename: request.name + ".json",
                conflictAction: "overwrite"
            })

            sendResponse({ result: "any response from background" });
        } else {
            sendResponse({ result: "error", message: `Invalid 'cmd'` });
        }

        return true;
    });